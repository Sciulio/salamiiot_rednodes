export var result = {
  nodes: Object.keys({
  ALARM_1: 24,
  ALARM_2: 25
}).map(function (_key) {
    return {
      "name": "GPIO:" + _key,
      "type": "gpio",
      "params": alarms[_key],
      "properties": {
      },
      "attributes": {
        "category": "output",
        "color": "green",
        "inputs": 1,
        "outputs": 1,
        "icon": "serial.png"
      },
      "tips": [
        "Read the temperature value."
      ],
      "tooltips": [
        "Read the temperature value."
      ]
    };
  })
};


/*var gigi = {
  ALARM_1: 24,
  ALARM_2: 25,
  ALARM_3: 5,
  ALARM_4: 6,
  ALARM_5: 4,
  ALARM_6: 27,
  ALARM_7: 10,
  ALARM_8: 9,
  ALARM_9: 11,
  ALARM_10: 8,
  ALARM_11: 7,
  ALARM_12: 19,
  ALARM_13: 16,
  ALARM_14: 20,
  ALARM_15: 21,
  ALARM_16: 12
};
var res = Object.keys(gigi)
.map(function(key) {
  var item = gigi[key];
  return {
    "name": "GPIO:" + key,
    "type": "gpio",
    "params": item,
    "properties": {
    },
    "attributes": {
      "category": "output",
      "color": "green",
      "inputs": 1,
      "outputs": 1,
      "icon": "serial.png"
    },
    "tips": [
      "Read the status of the GPIO " + key + "."
    ],
    "tooltips": [
      "Read the status of a GPIO port."
    ]
  };
});
console.log(JSON.stringify({nodes: res}))*/