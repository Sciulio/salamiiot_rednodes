var m_i2c = require("i2c");


var settings = {
    device: "/dev/i2c-1", // > rasp.1
    address: 0x08,
    getRespBytesLength: 1
};

var wire = null;

var _init = function () {
    if (!wire) {
        wire = new m_i2c(settings.address, {
            device: settings.device
        });
    }
};

var _responseToByte = function (res) {
    return Buffer.isBuffer(res) && settings.getRespBytesLength == 1 ? res[0] : res;
};

var _prepareMessage = function (oldmsg, value, transform, topicData) {
    var data = oldmsg || {};

    // set topic as string, array or object if passed
    if (typeof topicData !== "undefined" && topicData != null) {
        data.topic = topicData;
    }

    // transform value if transform function is passed
    if (transform && typeof transform === "function") {
        value = transform(value);
    }

    // set value to payload
    data.payload = value;

    return data;
}
/*
Legend for: command
    [ 0, 10[    : read N byte of passed arguments
    10          : positive payload logic
    11          : negative payload logic
    12          : toggle logic TODO
*/

var _module = {
    mergeAsHex: function(node, config, addresses, transform, topicData) {
        return function(msg) {
            
            /* BUG???
            Promise.all([
                i2cWrapper.readPromise(node, 25),
                i2cWrapper.readPromise(node, 32)
            ]).then(values => {
            */
            _module.readMulti(
                addresses,
                values => {
                    var payload = parseInt("0x" + values.map(val => val.toString(16)).join(""), 16);
                    node.send(_prepareMessage(msg, payload, transform, topicData));
                },
                reason => node.error(reason)
            );
        };
    },
    sendCommand: function (node, config, addresses, transform, topicData) {
        //var addresses = Array.prototype.slice.call(arguments).slice(2);

        return function (msg) {
            var addressIdx = 0;

            if (typeof config.command !== "undefined") {
                var command = parseInt(config.command);

                if (command < 10) {
                    addressIdx = command;
                } else if (command < 20) {
                    if (command == 10) {
                        addressIdx = msg.payload > 0 ? 0 : 1;
                    } else {
                        addressIdx = msg.payload <= 0 ? 0 : 1;
                    }
                } else {
                    node.log("Wrong arguments passed!");
                    return;
                }
            }

            if (addresses.length > addressIdx) {
                var cmd = addresses[addressIdx];
                node.log("Reading: " + cmd);

                _module.read(cmd, (value) => {
                    node.send(_prepareMessage(msg, value, transform, topicData));

                    node.log("Read: " + cmd);
                }, (err) => node.error);
            } else {
                node.log("Wrong arguments passed!");
            }
        };
    },
    /*
        BUGGED Promise implementation???:
        wire.readBytes returns always <Buffer 00>
    */
    readPromise: function (cmd) {
        return new Promise((resolve, reject) => {
            _module.read(cmd, resolve, reject);
        });
    },
    readMulti: function (cmdArray, cbackSuccess, cbackError) {
        var i_res = 0;
        var i_res_tot = cmdArray.length;
        var resArray = Array(i_res_tot);

        var _clear = function() {
            resArray.length = 0;
            resArray = null;
        };

        var resolve = function(_i) {
            return function(_res) {
                if (resArray == null) {
                    return;
                }

                resArray[_i] = _res;

                if (++i_res == i_res_tot) {
                    cbackSuccess(resArray);

                    _clear();
                }
            };
        };
        var reject = function(reason) {
            _clear();

            cbackError(reason);
        };

        for (var i = 0; i < cmdArray.length; i++) {
            _module.read(cmdArray[i], resolve(i), reject);
        }
    },
    read: function (cmd, cbackSuccess, cbackError) {
        _init();

        wire.readBytes(parseInt(cmd), settings.getRespBytesLength, function (err, res) {
            if (err) {
                cbackError && cbackError(err);
            } else {
                cbackSuccess && cbackSuccess(_responseToByte(res));
            }
        });
    }
};

module.exports = _module;