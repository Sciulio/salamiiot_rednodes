var settings = {
};

var _init = function () {
};

var _module = {
    readPromise: function (cmd) {
        return new Promise((resolve, reject) => {
            _module.read(cmd, resolve, reject);
        });
    },    
    read: function (cmd, cbackSuccess, cbackError) {
        _init();

        wire.readBytes(parseInt(cmd), settings.getRespBytesLength, function (err, res) {
            if (err) {
                cbackError && cbackError(err);
            } else {
                cbackSuccess && cbackSuccess(_responseToByte(res));
            }
        });
    }
};

module.exports = _module;