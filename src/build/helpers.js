module.exports.register = function (Handlebars, options)  {
  /**
   * strictly equal condition helper
   */
  Handlebars.registerHelper('ifEqual', function(v1, v2, options) {
    if(v1 === v2) {
      return options.fn(this);
    }
    return options.inverse(this);
  });

  /** */
  Handlebars.registerHelper('printAsJsonValue', function(key, value) {
    if (typeof value === 'undefined') {
      return "";
    }
    return key + ": " + (typeof value === "string" ? "'" + value + "'" : value) + ",";
  });

  /** */
  Handlebars.registerHelper('toJson', function(value) {
    if (typeof value === 'undefined') {
      return "null";
    }
    return JSON.stringify(value);
  });

  /** */  
  Handlebars.registerHelper('parseValue', function(value) {
    return typeof value === "string" ? "'" + value + "'" : value;
  });

  /** */
  Handlebars.registerHelper("valueOrNull", function(val) {
    return val && val !== 0 ? val : "null";
  });

  /** */  
  Handlebars.registerHelper('inputProperty', function(key, context) {
    var ret = "";
    var _vals = context.value;

    if (typeof _vals == "string" && context.value.startsWith("{{")) {
      _vals = [];
      bindValue(context.value)
      .forEach(function(item) {
        _vals = _vals.concat(item.value);
      });
    }

    if (Array.isArray(_vals)) {
      ret += htmlSelect(key, context, _vals);
    } else {
      ret += htmlText(key, context);
    }
    
    return ret;
  });

  var data = require("../../data/common.json");

  var bindValue = function(val) {
    val = val.substr(2, val.length - 4);
    vals = val.split(":");
    
    return data[vals[0]].filter(function(item) {
      return item[vals[1]] == vals[2];
    });
  };
  var htmlText = function(key, context) {
    return '<input type="text" id="node-input-' + key + '" placeholder="' + context.label + '"/>';
  };
  var htmlSelect = function(key, context, _vals) {
      var ret = '<select id="node-input-' + key + '">';
      
      for (var i = 0; i < _vals.length; i++) {
        var valg = _vals[i];

        if (Array.isArray(valg.value)) {
          ret += '<optgroup label="' + valg.label + '">';

          for (var j = 0; j < valg.value.length; j++) {
            var valo = valg.value[j];

            ret += '<option label="' + valo.label + '" value="' + valo.value + '">' + valo.label + '</option>';
          }
          
          ret += '</optgroup>';
        }
      }

      ret += '</select>';
      return ret;
  };
};