module.exports = function (grunt) {
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks("grunt-handlebars-layouts");

  
  grunt.registerTask('default', [
    'clean',
    'handlebarslayouts',
    'copy'
  ]);


  grunt.initConfig({
    clean: [
      'build'
    ],
    copy: {
      nodes: {
        files: [{
          expand: true,
          cwd: 'data/nodes',
          src: [
            '**',
            '!**/*.json'
          ],
          dest: 'build/',
          filter: 'isFile'
        }]
      }
    },
    handlebarslayouts: {
      options: {
        modules: [
          'src/build/helpers*.js'
        ]
      },
      date: {
        files: {
          'build/date/date.html': 'src/templates/html.hbs',
        },
        options: {
          context: [
            'data/nodes/date/date.json'
          ]
        }
      },
      bcv: {
        files: {
          'build/bcv/bcv.html': 'src/templates/html.hbs',
          'build/bcv/bcv.js': 'src/templates/js.hbs'
        },
        options: {
          context: [
            //'data/nodes/bcv/bcv.json'
            'data/nodes/bcv.json'
          ]
        }
      },
      analogic: {
        files: {
          'build/analogic/analogic.html': 'src/templates/html.hbs',
          'build/analogic/analogic.js': 'src/templates/js.hbs'
        },
        options: {
          context: [
            //'data/nodes/analogic/analogic.json'
            'data/nodes/analogic.json'
          ]
        }
      },
      readers: {
        files: {
          'build/readers/readers.html': 'src/templates/html.hbs',
          'build/readers/readers.js': 'src/templates/js.hbs'
        },
        options: {
          context: [
            'data/nodes/readers.json'
          ]
        }
      },
      gpios: {
        files: {
          'build/gpios/gpios.html': 'src/templates/html.hbs',
          'build/gpios/gpios.js': 'src/templates/js.hbs'
        },
        options: {
          context: [
            'data/nodes/gpios.json'
          ]
        }
      },
      semaphores: {
        files: {
          'build/semaphores/semaphores.html': 'src/templates/html.hbs',
          'build/semaphores/semaphores.js': 'src/templates/js.hbs'
        },
        options: {
          /*partials: [
            'src/partials/*.hbs',
            'src/layout.html'
          ],*/
          /*modules: [
            'src/build/helpers*.js'
          ],*/
          basePath: '',
          context: [
            'data/nodes/semaphores.json'
          ]
          /*
          context: grunt.file.readJSON('templates/nodes.json')
          context: {
            title: 'Layout Test',
            items: [
              'apple',
              'orange',
              'banana'
            ]
          }*/
        }
      }
    }
  });
};