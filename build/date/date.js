module.exports = function (RED) {
  "use strict";

  var i2cWrapper = require("../../src/nodes/i2cWrapper");

  RED.nodes.registerType("DATE_VALUE", function (config) {
    RED.nodes.createNode(this, config);
    var node = this;

    this.on("input", function (msg) {
      i2cWrapper.read(64, (res) => { // update datetime
        setTimeout(() => {
          i2cWrapper.readMulti([65, 66, 67, 68, 69, 70, 71], values => {
          /*Promise.all([
            i2cWrapper.readPromise(node, 0),
            i2cWrapper.readPromise(node, 14),
            i2cWrapper.readPromise(node, 17),
            i2cWrapper.readPromise(node, 2),
            i2cWrapper.readPromise(node, 18),
            i2cWrapper.readPromise(node, 10),
            i2cWrapper.readPromise(node, 16)
          ]).then(values => {*/
            // treat read data
            // values = [value1, ...]
            values = values.map((val) => parseInt(val));
            console.log(values)
            var data = {
              payload: new Date(2000 + values[6], values[5], values[4], values[2], values[1], values[0])
            };
            
            node.send(data);
          }, reason => node.error(reason)
          ), 10
        }), reason => node.error(reason)
      });
    });

    this.on('close', function () {
      // tidy up any state
    });
  });
};