module.exports = function(RED) {
    "use strict";
    
    var i2cWrapper = require("../../src/nodes/i2cWrapper");

      RED.nodes.registerType("ANALOGIC_READ_1", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.mergeAsHex(
            this,
            config,
            [96,97],
            function(value) { return value * 0.001; },
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("ANALOGIC_READ_2", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.mergeAsHex(
            this,
            config,
            [98,99],
            function(value) { return value * 0.001; },
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
};