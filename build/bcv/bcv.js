module.exports = function(RED) {
    "use strict";
    
    var i2cWrapper = require("../../src/nodes/i2cWrapper");

      RED.nodes.registerType("BATTERY_CHARGE_VALUE", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.mergeAsHex(
            this,
            config,
            [25,32],
            function(value) { return value * 0.001; },
            "Battery"
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
};