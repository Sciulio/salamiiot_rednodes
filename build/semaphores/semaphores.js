module.exports = function(RED) {
    "use strict";
    
    var i2cWrapper = require("../../src/nodes/i2cWrapper");

      RED.nodes.registerType("Salami RELE3", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [23,24],
            null,
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("Salami SEMR", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [35,36],
            null,
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("Salami SEMV", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [37,38],
            null,
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("Salami BUZZER", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [33,34],
            null,
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("BATTERY_MODE", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [21,22],
            null,
            null
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
};