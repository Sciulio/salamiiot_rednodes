module.exports = function(RED) {
    "use strict";
    
    var i2cWrapper = require("../../src/nodes/i2cWrapper");

      RED.nodes.registerType("TEMP_VALUE", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [49],
            function(value) { return value - 50; },
            "temperature"
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("VOLT_VALUE", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [50],
            null,
            {"topico":"volt"}
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
      RED.nodes.registerType("MAIN_SUPPLY_PRESENCE", function(config) {
        RED.nodes.createNode(this, config);
        var node = this;

          this.on("input", i2cWrapper.sendCommand(
            this,
            config,
            [51],
            null,
            "supply"
          ));
        
        this.on('close', function() {
            // tidy up any state
        });
      });
};